<?php

/**
 * Bit&Black InDesign Characters.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\InDesignCharacters;

use BitAndBlack\InDesignCharacters\Converter\ByteConverter;

/**
 * This class returns some whitespaces which can be accessed without initializing as object.
 * Even if they look like normal whitespaces, they are characters with different widths.
 */
class Space
{
    /**
     * @var array<int, int>
     */
    private static array $emSpace = [226, 128, 131];

    /**
     * @var array<int, int>
     */
    private static array $enSpace = [226, 128, 130];

    /**
     * @var array<int, int>
     */
    private static array $thirdSpace = [226, 128, 132];

    /**
     * @var array<int, int>
     */
    private static array $quarterSpace = [226, 128, 133];

    /**
     * @var array<int, int>
     */
    private static array $sixthSpace = [226, 128, 134];

    /**
     * @var array<int, int>
     */
    private static array $thinSpace = [226, 128, 137];

    /**
     * @var array<int, int>
     */
    private static array $hairSpace = [226, 128, 138];

    /**
     * @var array<int, int>
     */
    private static array $nonBreakingSpace = [194, 160];

    /**
     * @var array<int, int>
     */
    private static array $nonBreakingSpaceFixedWidth = [226, 128, 175];

    /**
     * @var array<int, int>
     */
    private static array $punctuationSpace = [226, 128, 136];

    /**
     * @var array<int, int>
     */
    private static array $figureSpace = [226, 128, 135];

    /**
     * @var array<int, int>
     */
    private static array $flushSpace = [226, 128, 129];
    
    /**
     * Returns a 1/1 space.
     *
     * @return string
     */
    public static function getEmSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$emSpace
        );
    }

    /**
     * Returns a 1/2 space.
     *
     * @return string
     */
    public static function getEnSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$enSpace
        );
    }

    /**
     * Returns a 1/3 space.
     *
     * @return string
     */
    public static function getThirdSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$thirdSpace
        );
    }

    /**
     * Returns a 1/4 space.
     *
     * @return string
     */
    public static function getQuarterSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$quarterSpace
        );
    }
    
    /**
     * Returns a 1/6 space.
     *
     * @return string
     */
    public static function getSixthSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$sixthSpace
        );
    }

    /**
     * Returns a 1/8 space.
     *
     * @return string
     */
    public static function getThinSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$thinSpace
        );
    }

    /**
     * Returns a 1/24 space.
     *
     * @return string
     */
    public static function getHairSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$hairSpace
        );
    }

    /**
     * Returns a non-breaking space.
     *
     * @return string
     */
    public static function getNonBreakingSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$nonBreakingSpace
        );
    }

    /**
     * Returns a non-breaking space with a fixed width.
     *
     * @return string
     */
    public static function getNonBreakingSpaceFixedWidth(): string
    {
        return ByteConverter::chrMulti(
            self::$nonBreakingSpaceFixedWidth
        );
    }

    /**
     * Returns a punctuation space.
     *
     * @return string
     */
    public static function getPunctuationSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$punctuationSpace
        );
    }

    /**
     * Returns a figure space.
     *
     * @return string
     */
    public static function getFigureSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$figureSpace
        );
    }

    /**
     * Returns a flush space.
     *
     * @return string
     */
    public static function getFlushSpace(): string
    {
        return ByteConverter::chrMulti(
            self::$flushSpace
        );
    }
}
