<?php

/**
 * Bit&Black InDesign Characters.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\InDesignCharacters\Converter;

class ByteConverter
{
    /**
     * Converts an array of numbers into a character.
     *
     * @param array<int, int> $numbers
     * @return string
     */
    public static function chrMulti(array $numbers): string
    {
        $numbers = array_map(
            static fn (int $number): string => chr($number),
            $numbers
        );
        
        return implode($numbers);
    }

    /**
     * Converts a characters to an array of numbers.
     *
     * @param string $character
     * @return array<int, int>
     */
    public static function ordMulti(string $character): array
    {
        $bytes = [];
        $charLength = strlen($character);
        
        for ($position = 0; $position < $charLength; ++$position) {
            $byte = substr($character, $position);
            $bytes[] = ord($byte);
        }
        
        return $bytes;
    }
}
