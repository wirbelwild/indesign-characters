<?php

/**
 * Bit&Black InDesign Characters.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\InDesignCharacters;

use BitAndBlack\InDesignCharacters\Converter\ByteConverter;

/**
 * This class returns some line breaks which can be accessed without initializing as object.
 * Even if they look like normal line breaks, they are characters with special meanings to Adobe InDesign.
 */
class BreakCharacter
{
    /**
     * @var array<int, int>
     */
    private static array $forcedLineBreak = [226, 128, 168];

    /**
     * Returns a forced line break.
     *
     * @return string
     */
    public static function getForcedLineBreak(): string
    {
        return ByteConverter::chrMulti(
            self::$forcedLineBreak
        );
    }
}
