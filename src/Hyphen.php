<?php

/**
 * Bit&Black InDesign Characters.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\InDesignCharacters;

use BitAndBlack\InDesignCharacters\Converter\ByteConverter;

/**
 * This class returns some hyphens which can be accessed without initializing as object.
 */
class Hyphen
{
    /**
     * @var array<int, int>
     */
    private static array $discretionaryHyphen = [194, 173];

    /**
     * Returns a discretionary hyphen.
     *
     * @return string
     */
    public static function getDiscretionaryHyphen(): string
    {
        return ByteConverter::chrMulti(
            self::$discretionaryHyphen
        );
    }
}
