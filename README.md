[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/indesign-characters)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/be375bf0a63244948f7435f6a832d36b)](https://www.codacy.com/bb/wirbelwild/indesign-characters/dashboard)
[![Total Downloads](https://poser.pugx.org/bitandblack/indesign-characters/downloads)](https://packagist.org/packages/bitandblack/indesign-characters)
[![License](https://poser.pugx.org/bitandblack/indesign-characters/license)](https://packagist.org/packages/bitandblack/indesign-characters)

# InDesign Characters

Characters used by Adobe InDesign. Helpful when [creating IDML files](https://idml-creator.bitandblack.com).

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/indesign-characters). Add it to your project by running `$ composer require bitandblack/indesign-characters`.

## Usage 

This library provides a collection of special characters used by Adobe InDesign. At the moment there are:

-   __Spaces__ like the em space, or the hair space
-   __Breaks__ like the forced line break
-   __Hyphens__ like the discretionary hyphen

You can access those characters statically, for example by calling:

```php
<?php

use BitAndBlack\InDesignCharacters\Space;

var_dump(Space::getHairSpace());
```

Don't worry if those characters look like normal whitespaces or linebreaks: they'll do their work.

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).