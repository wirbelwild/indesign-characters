# Changes in InDesign Characters 1.0

## 1.0.0 2022-11-21

### Changed 

-   PHP >=7.4 is now required.
-   The namespace changed from `InDesignCharacters` to `BitAndBlack\InDesignCharacters`.