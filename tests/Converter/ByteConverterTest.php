<?php

/**
 * Bit&Black InDesign Characters.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\InDesignCharacters\Tests\Converter;

use BitAndBlack\InDesignCharacters\Converter\ByteConverter;
use Generator;
use PHPUnit\Framework\TestCase;

class ByteConverterTest extends TestCase
{
    /**
     * @dataProvider getConversionData
     * @param string $character
     * @param array<int, int> $bytes
     * @return void
     */
    public function testConvertsCorrectly(string $character, array $bytes): void
    {
        self::assertEquals(
            ByteConverter::ordMulti($character),
            $bytes
        );

        self::assertEquals(
            ByteConverter::chrMulti($bytes),
            $character
        );
    }

    public function getConversionData(): Generator
    {
        yield [
            '🐘',
            [240, 159, 144, 152],
        ];
    }
}
